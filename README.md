# AquaStops

## One-Sentence-Pitch

Prevent immersion fluid and media from seeping into your microscope with single objective aqua-stops. 


## How does it work?

<img src="AquastopvsHairthingy2.jpg" width=100% />

I have developed prototypes for single-objective aqua-stops, which are specifically designed to prevent immersion solution from infiltrating the microscope body via the objective's screw-in surface. Currently, the prevalent methods for this task involve either utilizing large, expensive, and unwieldy commercial aqua-stops or resorting to hair band elastic as an improvised absorbent for any leakage of the immersion liquid.

In contrast, my solution employs one (or possibly two) 2mm o-ring(s) to counteract water ingress. The model is fabricated from PETG, a choice motivated by the necessity for slight material flexibility, which facilitates a tight fit that is crucial for achieving a flawless seal. Moreover, the plastic demonstrates resilience against the immersion liquids used.

To assemble, I trim the o-ring down to the required length and fasten the ends together to form a butt joint, utilizing standard cyanoacrylate adhesive (commonly known as "super glue").

As a demo I also created a protective cover for the Leica HC PL APO 20x0.75 objective:

<img src="LeicaCPLANAPO20x07.jpg" width=55% />

It protects the lens when it is not in use. This proved useful as in my experience, the condition of the 20x dry objectives is the worst, as they are in danger of getting hit by immersion bottles/liquids because they are situated next to immersion objectives. 
 I minimized the protruding height of the cap, by reducing the thickness down to 240 mu. Hence, the cap needs to be printed upside down. 

## Status

Both of these designs are currently functional, yet there's room for improvement in daily usage. I plan to introduce more designs targeting various other objectives. 

This concept is especially interesting with objectives with correction collar: 


<img src="CAPO40x.jpg" width=55% />


Here a two-piece aqua-stop could help to be able to a) protect from water ingress and b) nevertheless enable the user to adjust and read the collar setting. 
Moreover, no aqua-stops are also available for objectives that feature motorized correction collars.

 Generally, I believe there's an abundance of opportunities worth investigating.
 

## Acknowledgment

This project has been made possible in part by grant number 2020-225401 from the Chan Zuckerberg Initiative DAF, an advised fund of Silicon Valley Community Foundation.